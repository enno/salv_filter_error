# salv_filter_error

in some ABAP Release below 750 there is the following error in cl_salv_table method set_data:
If a filter or sorting is applied to one column, then after set_data the filter or sorting will still be displayed. With sorting the little red triangle to visualize the sorting order will not be displayed but sorting is still active.