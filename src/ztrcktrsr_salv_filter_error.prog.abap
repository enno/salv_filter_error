REPORT.

" check program to find out if set_data works correctly.
" 1. start program
" 2. set sorting or filter on column 1 to 3
" 3. switch to 2nd view
" 4. check whether filter or sorting still affects the display of the 2nd salv


TYPES: BEGIN OF ts_data_1,
         matnr TYPE matnr,
         werks TYPE werks_d,
         lgort TYPE lgort_d,
       END OF ts_data_1.
TYPES: BEGIN OF ts_data_2,
         vbeln TYPE vbeln,
         vkorg TYPE vkorg,
         vtweg TYPE vtweg,
         spart TYPE spart,
       END OF ts_data_2.

DATA data_table_1 TYPE STANDARD TABLE OF ts_data_1.
DATA data_table_2 TYPE STANDARD TABLE OF ts_data_2.
DATA option TYPE string VALUE 'ONE'.


SELECTION-SCREEN PUSHBUTTON /1(20) b01 USER-COMMAND one MODIF ID one.
SELECTION-SCREEN PUSHBUTTON /1(20) b02 USER-COMMAND two MODIF ID two.


INITIALIZATION.
  b01 = 'Table one' ##no_text.
  b02 = 'Table two' ##no_text.


  data_table_1 = VALUE #(
    ( matnr = 'A01' werks = '1000' lgort = '0010' )
    ( matnr = 'A02' werks = '1000' lgort = '0010' )
    ( matnr = 'A03' werks = '1000' lgort = '0030' )
    ( matnr = 'B01' werks = '2000' lgort = '0010' )
    ( matnr = 'C01' werks = '1000' lgort = '0010' )
    ( matnr = 'C02' werks = '3000' lgort = '0020' )
  ).

  data_table_2 = VALUE #(
  ( vbeln = '0000000100' vkorg = '1000' vtweg = '10' spart = '01' )
  ( vbeln = '0000000101' vkorg = '1000' vtweg = '10' spart = '01' )
  ( vbeln = '0000000102' vkorg = '1000' vtweg = '60' spart = '01' )
  ( vbeln = '0000000104' vkorg = '2000' vtweg = '10' spart = '01' )
  ( vbeln = '0000000105' vkorg = '2000' vtweg = '10' spart = '01' )
  ( vbeln = '0000000109' vkorg = '6000' vtweg = '60' spart = '01' )
   ).

  cl_salv_table=>factory(
      EXPORTING
        r_container    = NEW cl_gui_docking_container( ratio = 80 side = cl_gui_docking_container=>dock_at_bottom )
      IMPORTING
        r_salv_table   = DATA(grid)
      CHANGING
        t_table        = data_table_1 ).
  grid->get_functions( )->set_all( ).
  grid->display( ).




AT SELECTION-SCREEN OUTPUT.

  LOOP AT SCREEN.
    IF option = screen-group1.
      screen-input = '0'.
      MODIFY SCREEN.
    ENDIF.
  ENDLOOP.

AT SELECTION-SCREEN.
  CASE sy-ucomm.
    WHEN 'ONE'.
      option = sy-ucomm.
      grid->set_data( CHANGING t_table = data_table_1 ).
      grid->refresh( ).
    WHEN 'TWO'.
      grid->set_data( CHANGING t_table = data_table_2 ).
      grid->refresh( ).
      option = sy-ucomm.
  ENDCASE.
